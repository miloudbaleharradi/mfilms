<?php
class Bootstrap{
    private $controller;
    private $action;
    private $request;

    public function __construct($request){
        $this->request = $request;
        if($this->request['controller'] == ''){
            $this->controller = 'home';
        }else{
            $this->controller = $this->request['controller'];
        }
        if($this->request['action'] == ''){
            $this->action = 'index';
        }else{
            $this->action = $this->request['action'];
        }
        // echo '<pre>'; print_r($request); echo '</pre>';
        // echo $this->action;
        // echo $this->controller;

    } 
    public function createController(){
        //check if class exists
        if(class_exists($this->controller)){
            $parents = class_parents($this->controller);
            //check Extend
            if(in_array('Controller', $parents)){
                if(method_exists($this->controller, $this->action)){
                    return new $this->controller($this->action, $this->request);
                }else{
                    //Method doesn't exist
                    echo '<h1> method does not exist </h1>';
                    return;
                }
            }else{
                //the base controller is not found
                echo '<h1>The base controller is not found </h1>';
                return;
            }
        }else{
            //the controller does not exist
            echo '<h1> the controller class does not exist</h1>';
            return;
        }
    }
}