<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
    integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
    integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" />
  <link rel="stylesheet" href="assets/css/style.css">
  <title>MiloudFlix</title>
</head>

<body>
    <!-- NAVBAR GUEST START HERE  -->
    <nav class="navbar navbar-expand-lg navbar-dark  bg-dark ">
        <a href="<?php echo ROOT_URL; ?>" class="navbar-brand">
            <h4 class="text-danger">MILOUDFLIX</h4>
        </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a href="<?php echo ROOT_URL; ?>home" class="nav-link">accueil</a>
            </li>
            <li class="nav-item dropdown">
                <div class="dropdown ">
                    <a class="dropdown-toggle nav-link" href="#" id="dropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        Categories
                    </a>
                    <div class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuLink">

                    </div>
                </div>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">Contact</a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a href="<?php echo ROOT_URL; ?>user/register" class="nav-link"><i class="fas fa-user"></i>
                S'inscrire</a>
            </li>
            <li class="nav-item">
                <a href="<?php echo ROOT_URL; ?>user/login" class="nav-link" ><i class="fas fa-sign-in-alt"></i>
                connection</a>
            </li>
            <li class="nav-item">
                <form class="form-inline my-2 ml-4 my-lg-0" name="search-form" id="search-form" method="GET" >
                    <input class="form-control mr-sm-1" type="search" size="10" placeholder="Search" aria-label="Search" name="search-input" id="search-input">
                    <a href="#">
                        <button class="btn btn-outline-danger my-2 my-sm-0" type="submit" name="search-btn" id="search-btn">go</button>
                    </a>
                </form>
            </li>
        </ul>
    </div>
    </nav>
    <!-- NAVBAR GUEST END HERE  -->

    <!-- INCLUDE VIEWS -->
    <?php require $view; ?>
    
    <!-- COPYRIGHT FOOTER  -->
    <!-- FOOTER CP RIGHT-->
    <footer id="main-footer" class="text-center bg-dark p-4 footer">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p>Copyright &copy;
                        <span id="year"></span> Miloud Baleharradi</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- FOOTER CP RIGHT-->
    <script src="http://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
    integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
    crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
    integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
    crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
    <script src="assets/js/app.js"></script>
</body>
</html>